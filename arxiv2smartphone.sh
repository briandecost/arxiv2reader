#!/bin/bash

# reference: https://tex.stackexchange.com/questions/16735/latex-options-for-kindle
# https://nathangrigg.com/2012/04/send-emails-from-the-command-line

# identify main LaTeX source file
TEXFILE=$(grep -l begin{document} *.tex)
echo found main tex file ${TEXFILE}

# insert geometry and microtype calls
# TODO: check to make sure there is no conflicting geometry include

# for iphone 5
# gawk -i inplace '/begin\{document\}/{print "\\usepackage[margin=3mm,a6paper]{geometry}"}1' ${TEXFILE}

# for iphone 6
# gawk -i inplace '/begin\{document\}/{print "\\usepackage[papersize={5.86cm,10.4cm},hmargin=0.5cm,vmargin={0.5cm,0.5cm}]{geometry}"}1' ${TEXFILE}

# for kindle
gawk -i inplace '/begin\{document\}/{print "\\usepackage[papersize={9cm,12cm},hmargin=0.5cm,vmargin={0.5cm,0.5cm}]{geometry}"}1' ${TEXFILE}

gawk -i inplace '/begin\{document\}/{print "\\usepackage{microtype}"}1' ${TEXFILE}
gawk -i inplace '/begin\{document\}/{print "\\pagestyle{empty}"}1' ${TEXFILE}

# build pdf
pdflatex ${TEXFILE}
pdflatex ${TEXFILE}
