#!/bin/bash
# this endpoint yields tarred and gzipped LaTeX source
# if available, otherwise PDF.
ARXIV_ID=1801.04016
BUILD_DIR=build-${ARXIV_ID}
mkdir ${BUILD_DIR}
cd ${BUILD_DIR}

curl https://arxiv.org/e-print/${ARXIV_ID} -o ${ARXIV_ID}.tar.gz
tar xvzf ${ARXIV_ID}.tar.gz
